# Formail
![email-send-outline](docs/assets/email-send-outline_black.png)
> API-Backend to validate and forward form data from client-side only websites via email.

If you're designing client-side only websites, the only backend you need is a simple webserver to serve all these static files.
Now consider you want to have a simple form on this site, but don't want to deploy a custom backend for just a small form.
This is where Formail comes in:

Create an endpoint and define:

- validation rules
- fields
- receivers
- cors origin headers
- rate limit
- and your SMTP credentials

This will generate a custom ajax endpoint which will do the following:

- check the rate limit
- validate the input
- forward the input to the defined email addresses

With Formail you don't have care about the security and availability of your backend. Everything is taken care of.
Even preflight (OPTIONS) requests and CORS-Headers are supported.

Register at [formail.dev](https://formail.dev).

Documentation at [docs.formail.dev](https://docs.formail.dev)
