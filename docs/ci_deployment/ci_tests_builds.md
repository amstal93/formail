# CI, Tests, Builds

The project is hosted on gitlab and it uses gitlab pipelines.
The pipelines are used to automatically run all tests, build and push the docker images. 
In addition to this, the documentation is built and deployed to gitlab pages every time they have been changed.

The docker images are only built and pushed if the tests run without any errors.

??? note "Continuous Deployment not activated"
    At the moment, the continuous deployment jobs are not activated due to a problem with the formail production setup.

All CI/CD files are located in the `cicd` directory.

## Container Registry

All images are pushed to the project's container registry: [https://gitlab.com/aakado/formail/container_registry](https://gitlab.com/aakado/formail/container_registry).

The images can be pulled like this:
```sh
docker pull registry.gitlab.com/aakado/formail:latest
```

## Tests

The project is tested extensively using several unit- and feature-tests.
All tests are executed as the first job of each pipeline.
If this job fails, the following jobs aren't executed. A branch with failing tests should not be merged!

## Documentation

The documentation is written in markdown, built using [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) and deployed using gitlab pages.

## Images

Three different kinds of images are pushed to the container registry.
Here is the explanation for the different images and the namings of them.

| Tag               |  Description                                                              |
|-------------------|---------------------------------------------------------------------------|
| latest            | Latest image of the Formail application.                                  |
| branch            | Latest image of the Formail application on the named branch.              |
| COMMIT_HASH       | Image of the Formail application at the given commit.                     |
| nginx-latest      | Latest image of the nginx configured for Formail.                         |
| nginx-branch      | Latest image of the nginx configured for Formail on the named branch.     |
| nginx-COMMIT_HASH | Image of the nginx configured for Formail at the given commit.            |
| dev-latest        | Latest image of the Formail development environment.                     |
| dev-branch        | Latest image of the Formail development environment on the named branch. |
| dev-COMMIT_HASH   | Image of the Formail development environment at the given commit.         |


!!! attention "latest does really mean 'latest'"
    After every build, regardless of the branch, the built image is tagged and pushed with the `latest`-tag.
    Due to that, for a production deployment, you should use the `master` images or a certain `COMMIT_HASH` image.
    `latest` doesn't mean stable! 
