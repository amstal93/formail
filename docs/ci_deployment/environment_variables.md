# Environment Variables
This page explains all environment variables and how they are used within the application.

| Name               | Description                                                        | Notes                                                                        |
|--------------------|--------------------------------------------------------------------|------------------------------------------------------------------------------|
| APP_NAME           | The name of the app.                                               |                                                                              |
| APP_ENV            | The environment in which the app is running.                       | E.g. local, testing, production                                              |
| APP_KEY            | A secret key used by laravel for encryption tasks.                 | [Read more](https://laravel.com/docs/6.x/encryption#configuration)           |
| APP_DEBUG          | Whether the application should show exceptions to the user or not. |                                                                              |
| APP_URL            | The URL to access the app.                                         |                                                                              |
| APP_HTTPS          | Everything is forced to use https if set true.                     |                                                                              |
| LOG_CHANNEL        | The default log channel laravel should use.                        |                                                                              |
| DB_CONNECTION      | Database driver.                                                   | Only PostgresSQL and SQLite drivers are installed in the docker images.      |
| DB_HOST            | Hostname of the database.                                          | The service name of the database container.                                  |
| DB_PORT            | Port of the database.                                              | 5432 with PostgresSQL.                                                       |
| DB_DATABASE        | Name of the database.                                              |                                                                              |
| DB_USERNAME        | Database user.                                                     |                                                                              |
| DB_PASSWORD        | Database password.                                                 |                                                                              |
| BROADCAST_DRIVER   | Not required at the moment.                                        |                                                                              |
| CACHE_DRIVER       |                                                                    | [Read more](https://laravel.com/docs/6.x/cache)                              |
| QUEUE_CONNECTION   |                                                                    | [Read more](https://laravel.com/docs/6.x/queues)                             |
| SESSION_DRIVER     |                                                                    | [Read more](https://laravel.com/docs/5.5/session)                            |
| SESSION_LIFETIME   |                                                                    |                                                                              |
| REDIS_HOST         | Hostname of the Redis server.                                      | The service name of the Redis container.                                     |
| REDIS_PASSWORD     | Redis password.                                                    |                                                                              |
| REDIS_PORT         | Redis port.                                                        |                                                                              |
| MAIL_DRIVER        | Mail driver.                                                       |                                                                              |
| MAIL_HOST          | Host of your SMTP server.                                          |                                                                              |
| MAIL_PORT          | SMTP Port.                                                         |                                                                              |
| MAIL_USERNAME      | Username for your SMTP server.                                     |                                                                              |
| MAIL_PASSWORD      | Password for your SMTP server.                                     |                                                                              |
| MAIL_ENCRYPTION    | Encryption method for your SMTP connections.                       |                                                                              |
| MAIL_FROM_ADDRESS  | Address which is used to send emails as.                           |                                                                              |
| MAIL_FROM_NAME     | Name which is used to send emails as.                              |                                                                              |
| SENTRY_LARAVEL_DSN | The DSN of your sentry project.                                    | [Read more](https://docs.sentry.io/error-reporting/quickstart/?platform=php) |


!!! note "Default Laravel Configuration"
    Most environmentvariables are predefined due to the laravel framework. [Read more](https://laravel.com/docs/6.x/configuration)
