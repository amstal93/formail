# Security

## Laravel Built-In Security Features

Laravel provides a big range of built-in security features.
Formail uses them to protect the application and its users.
Laravel provides a default authentication mechanism, from which I made use of.

!!! danger "*Generate a secret app key*"
    > Before using Laravel's encrypter, you must set a key option in your `config/app.php` configuration file.
      You should use the `php artisan key:generate` command to generate this key since this Artisan command will use PHP's secure random bytes generator to build your key.
      If this value is not properly set, all values encrypted by Laravel will be insecure. [^5]
    
    Most of the built-in security features are only as secure as the app key of the running application.
    Read more about generating an app key with formail in this section: [Generate an app key](../ci_deployment/deployment.md#app-key).

## Session Management
This authentication mechanism includes secure session management, as after every significant state change like log in or logout, the session is regenerated or destroyed.
In addition to this, all cookies are encrypted and signed. Invalid cookies will be discarded automatically.
Therefore, reading or manipulating cookies is not possible.

### Change Session after significant state changes

- [Flush session after logout](https://github.com/laravel/framework/blob/4e33586e6cf6baf3a857e8ec6bd6de2ae4f93ad9/src/Illuminate/Session/Middleware/AuthenticateSession.php#L92)
- [Regenerate session after login](https://github.com/laravel/framework/blob/4e33586e6cf6baf3a857e8ec6bd6de2ae4f93ad9/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php#L105)

### Signed and encrypted cookies

> All cookies created by the Laravel framework are encrypted and signed with an authentication code, meaning they will be considered invalid if they have been changed by the client. [^1] 

## SQL-Injection

Laravel uses Eloquent as default ORM.
Eloquent protects against SQL injection as long as the so-called [Query Builder](https://laravel.com/docs/6.x/queries) is used for queries. 
Owing to this provided protection, Formail uses the Query Builder for every database query.

> The Laravel query builder uses PDO parameter binding to protect your application against SQL injection attacks. There is no need to clean strings being passed as bindings. [^2]  

## XSS / Cross-Site-Scripting

Formail is also protected against XSS / Cross-Site-Scripting, as it escapes every output with the blade template engine.

> Blade `{{ }}` statements are automatically sent through PHP's htmlspecialchars function to prevent XSS attacks. [^3]

## CSRF   
Blade also offers an easy to use protection against csrf attacks.
In Formail, every form is provided with the `@csrf` field. [^6]

## Input Validation and Mass Assignment

Every input is validated against various validation rules.
For this, the [laravel validation](https://laravel.com/docs/6.x/validation) is used.

Laravel also provides a protection against [mass assignment vulnerabilities](https://cheatsheetseries.owasp.org/cheatsheets/Mass_Assignment_Cheat_Sheet.html).
Formail uses this protection with so-called `whitelisting`. [Read more](https://laravel.com/docs/6.x/eloquent#mass-assignment).

## Restrictions for the database user
At the moment the laravel database user is only restricted to the formail schema.
So the user has only access to one schema, but within this schema, he can do everything. 
He isn't restricted to the `SELECT, INSERT, UPDATE, DELETE` operations.

## Error Handling

Exception handling is already configured properly in every new laravel project.
In production environments, error pages only contain a short description of the error and the HTTP status code.
Therefore, the user can't see anything besides a comprehensible error message with no details.
A detailed error page is only showed in development environments, where the `APP_DEBUG` environment variable is set to false. [^4]

All errors, also production errors, are forwarded to the configured sentry project.
This means every thrown error in formail will be noticed.

## Sending mails with formail
With Formail, the users can send emails with custom-defined credentials, including a host and a port.
This hosts and port are validated with the following rules:

- Allowed Ports:
    - 25, 2525, 465, 587
- Allowed Hosts:
    - All hosts matching the following regex pattern are allowed:
    - ```regexp
      ^((?:([a-z0-9]\.|[a-z0-9][a-z0-9\-]{0,61}[a-z0-9])\.)+)([a-z0-9]{2,63}|(?:[a-z0-9][a-z0-9\-]{0,61}[a-z0-9]))\.?$
      ```

!!! danger
    Due to this, the deployment has to be secured, that there are no mail servers internally available on the allowed ports.

## Store sensitive information encrypted in the database
At the moment, the only sensitive user information stored with formail is the SMTP passwords of each user.

To secure this information in case of a database leak/breach or to just hide it from DBA's or developers, such information is stored encrypted.
Every Model with sensitive information uses the `App\Traits\Encryptable.php` trait and defines the encrypted values inside the `$encryptable` array.
This solution is based on the [laravel encryption](https://laravel.com/docs/6.x/encryption).
The source of the code is copied from this StackOverflow answer: [Encrypt/Decrypt DB fields in laravel](https://stackoverflow.com/a/52024402/7130107).
 

## Official deployment
The official Formail deployment is hosted with a `.dev` top-level domain.
>We're making the web safer one .dev at a time. Every .dev domain is on the HSTS preload list, which makes HTTPS required on all connections. That means built-in security for you and your customers. [^7]

Therefore, only `https` is allowed.
Automatic [Let’s Encrypt](https://letsencrypt.org/) are configured for all used domains (also for the documentation).


## More Information
More Information about Laravel Security can be found here:

- [Laravel Security – Best Practices To Protect Your Web App](https://asperbrothers.com/blog/laravel-security/)
- [Why Laravel is the Recommended Framework for Secure, Mission-Critical Applications](https://auth0.com/blog/why-laravel-is-the-recommended-framework-for-secure-mission-critical-applications/)
- [Laravel Security](https://laravel.com/docs/6.x/authentication)

[^1]: [Retrieving Cookies From Requests](https://laravel.com/docs/6.x/requests#cookies)
[^2]: [Database: Query Builder](https://laravel.com/docs/6.x/queries#introduction)
[^3]: [Displaying Data](https://laravel.com/docs/6.x/blade#displaying-data)
[^4]: [Configure Laravel Error Handling](https://laravel.com/docs/6.x/errors#configuration)
[^5]: [Laravel app key](https://laravel.com/docs/6.x/encryption#configuration)
[^6]: [Blade CSRF Field](https://laravel.com/docs/6.x/blade#csrf-field)
[^7]: [Built-in security](https://domains.google/tld/dev/)
