<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$endpoint->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
</head>
<body>
<div
    style="width:100%;padding:24px 0 24px 0;background-color:#fafafa;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
    <div
        style="width:90%;max-width:680px;min-width:280px;border:1px solid #dddddd;background-color:#ffffff;margin:auto">
        <div style="color:#505050;font-size:13px;padding:24px">
            <div
                style="font-size:30px;line-height:34px;border-bottom:1px solid #dddddd;margin-bottom:32px;padding-bottom:8px">
                {{$endpoint->name}}
            </div>
            <ul style="padding:0;list-style-type:none">
                @foreach($entries as $entry)
                    <li style="list-style-type:none">
                        <h5 style="background-color:#eeeeee;padding:4px;font-weight:bold;font-size:13px;margin:0">{{$entry['name']}}
                            ({{$entry['keyword']}})</h5>
                        <p style="margin:4px 4px 16px 4px">{{$entry['value']}}</p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
</body>
</html>
