@extends('layouts.app')

@section('content')
    <header class="container d-flex justify-content-between mb-4">
        <h4>My Entries</h4>
        <a class="btn bg-primary" href="/entries/create">Create Entry</a>
    </header>
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Name</th>
                            <th scope="col">Keyword</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($entries as $entry)
                            <tr>
                                <td>
                                    <a href="/entries/{{$entry->id}}">
                                        <button type="submit" class="btn btn-success btn-sm">Edit</button>
                                    </a>
                                </td>
                                <td>
                                    <form method="POST" action="/entries/{{$entry->id}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                                <td>{{$entry->name}}</td>
                                <td>{{$entry->keyword}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
@endsection
