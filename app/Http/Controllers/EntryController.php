<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Rules\EntryBelongsToUser;

class EntryController extends Controller
{
    public function create()
    {
        return view('entries.create', [
            'entry' => new Entry(),
            'validations' => auth()->user()->validations
        ]);
    }

    public function store()
    {
        $entry = auth()->user()->entries()->create($this->validateRequest());

        return redirect('/entries/' . $entry->id);
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => ['required', 'max:255'],
            'keyword' => ['required', 'string', 'regex:/^[a-zA-Z0-9-_]+$/'],
            'validation_id' => ['required', 'exists:validations,id', new EntryBelongsToUser('validations')],
        ]);
    }

    public function index()
    {
        $entries = auth()->user()->entries;

        return view('entries.index', compact('entries'));
    }

    public function show(Entry $entry)
    {
        if (auth()->user()->isNot($entry->user)) {
            abort(403);
        }
        return view('entries.show', [
            'entry' => $entry,
            'validations' => auth()->user()->validations
        ]);
    }

    public function destroy(Entry $entry)
    {
        if (auth()->user()->isNot($entry->user)) {
            abort(403);
        }
        Entry::destroy($entry->id);

        return redirect('/entries');
    }

    public function update(Entry $entry)
    {
        if (auth()->user()->isNot($entry->user)) {
            abort(403);
        }

        $entry->update($this->validateRequest());

        return redirect('/entries/' . $entry->id);
    }

}
