<?php


namespace App\Traits;

/**
 * Trait Encryptable
 * This trait allows models to encrypt attributes before storing them in the database and to decrypt them right after reading them from the database.
 * The encryptable attributes have to be mentioned in a array called $encryptable.
 * Source: https://stackoverflow.com/a/52024402/7130107
 * @package App\Traits
 */
trait Encryptable
{
    /**
     * If the attribute is in the encryptable array
     * then decrypt it.
     *
     * @param  $key
     *
     * @return $value
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if (in_array($key, $this->encryptable) && $value !== '') {
            if ($value) {
                $value = decrypt($value);
            }
        }
        return $value;
    }

    /**
     * If the attribute is in the encryptable array
     * then encrypt it.
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable)) {
            if ($value) {
                $value = encrypt($value);
            }
        }
        return parent::setAttribute($key, $value);
    }

    /**
     * When need to make sure that we iterate through
     * all the keys.
     *
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();
        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])) {
                $attributes[$key] = decrypt($attributes[$key]);
            }
        }
        return $attributes;
    }
}
