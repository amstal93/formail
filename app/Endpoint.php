<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Endpoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'cors_origin', 'subject', 'monthly_limit', 'client_limit', 'time_unit', 'credential_id', 'path'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if (!isset($model->path)) {
                $path = Endpoint::createRandomString();
                $model->path = $path;
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function credential()
    {
        return $this->belongsTo(Credential::class);
    }

    /**
     * @return BelongsToMany
     */
    public function receivers()
    {
        return $this->belongsToMany(Receiver::class)->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function entries()
    {
        return $this->belongsToMany(Entry::class)->withTimestamps();
    }

    /**
     * @return string
     * @throws Exception
     */
    private static function createRandomString(): string
    {
        do {
            $path = bin2hex(random_bytes(20));
            $notUnique = Endpoint::where(['path' => $path])->exists();
        } while ($notUnique);
        return $path;
    }

}
