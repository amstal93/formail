<?php

namespace App\Providers;

use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Swift_Mailer;
use Swift_SmtpTransport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Swift_Mailer::class, function ($app, $config) {
            $transport = new Swift_SmtpTransport($config['host'], $config['port']);
            $transport->setUsername($config['username']);
            $transport->setPassword($config['password']);
            $transport->setEncryption($config['encryption']);

            $swift_mailer = new Swift_Mailer($transport);

            $mailer = new Mailer($app->get('view'), $swift_mailer, $app->get('events'));
            $mailer->alwaysFrom($config['from_address'], $config['from_name']);
            return $mailer;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.https')) {
            URL::forceScheme('https');
        }
    }
}
